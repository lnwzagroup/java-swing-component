/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.javaswingcomponent;

/**
 *
 * @author Gigabyte
 */
import java.awt.*;

class IconExample {

    IconExample() {
        Frame f = new Frame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\119139409_937936073280560_8682755590890563049_n.jpg");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample();
    }
}
